# Task A: Descriptive Analytics

Your client, a large South African telecommunications firm, has requested your assistance in creating a new management report for their cellular division.  Their current reporting is manually compiled from a variety of manually enriched system extracts, but they have decided to move away from this as it is slow and prone to many data errors (both user and from source). 

## Dataset

The client has provided you the following raw and unfiltered system extracts to compile the proof of concept:
-	An extract from their user system, containing customer details, extracted from their billing system (you are told that the records here should be unique)
-	An extract from their cellular mapping table, linking customers to devices, extracted from their marketing system (you are told that every device should have an owner)
-	An extract from their towers data, containing tower details (you are told that each should be associated with a unique numeric id)
-	An extract from their call logs

## Client Requirements

As an output, they would like to see:
-	A `Python` script that receives the above inputs and generates the required key metrics
-   Please refer to the *existing* Python files to add your code to generate these metrics

### Key metrics

**Overview**
-	No. of Customers per customer region
-	No. of Devices per customer region
-	No. of Towers per customer region
-	No. of Calls received per customer region
-	No. of Calls made per customer region
-	Revenue per customer region

**Customer View**
-	No. of Devices per customer
-	No. of Calls per customer (from/to)
-	No. of Regions called per customer (from/to)
-	No. of Towers called per customer (from/to)
-	Total Revenue per customer

### Email Summary

A draft of an e-mail to their operational lead, including your findings  and your professional opinion of the data (and underlying pricing plan) – are there any interesting findings, areas of concern etc

* Write your email in the `.md` mail file provided.

> Please document all data quality / cleaning assumptions made in the `assumptions.md` file.

## Appendix - Charge Sheet

- Please Note: The call rate is doubled for the basic package if they exceed 300 minutes / 40 calls
- Please Note: The call rate is multiplied by 1.5 for calls to another region – excluding the VIP package where it is not modified

### Base Charges

| Type   | Flat Fee |
|--------|----------|
| VIP    | 500      |
| Normal | 200      |
| Basic  | 0        |

### Usage Charges

**All charges below are per minute**

| Type   | Basic | Normal | VIP  |
|--------|-------|--------|------|
| VIP    | 0.05  | 0.05   | Free |
| Normal | 0.15  | 0.15   | 0.15 |
| Basic  | 0.30  | 0.30   | 0.30 |
