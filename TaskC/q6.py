import numpy as np

def shortest_path(grid: np.array) -> np.array:
    """
    Calculates the shortest path between a start and ending position within a grid (array).

    Args:
    ------
        grid:  2d array with the start and end positions

    Returns:
    --------
        numpy.array: A 2d array of the shortest path co-ordinates, the first column is the ith position
        and the second column is the jth position of each step
    """
    shortest_path = None
    # START CODE HERE

    # END CODE HERE
    return shortest_path

