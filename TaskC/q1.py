import numpy as np

def newton(f, Df, x0, epsilon=1e-9, max_iter=1e9):
    '''Approximate solution of f(x)=0 by Newton's method.

    Args:
    ------
        f (list): Function for which we are searching for a solution f(x)=0.
        Df (list): Derivative of f(x).
        x0 (int/float) Initial guess for a solution f(x)=0.
        epsilon (float): Stopping criteria is abs(f(x)) < epsilon.
        max_iter (int): Maximum number of iterations of Newton's method.

    Returns:
    -------
        float/none: if early stop return none else xn (solution to root solve)
    '''
    xn = None
    # START CODE HERE


    # END CODE HERE
    return xn


def calculate_irr(cf: np.array, r0: float) -> float:
    """
    Calculate the internal rate of return (irr)
    of an investment per period provided, using Newton's Method above.
    
    Args:
    ------
        cf (numpy.array): The array of cash flows
        r0 (float): The estimate of the irr (note not x)
        
    Returns:
    --------
        float: The calculated irr over the life of the investment
        per period.
    
    """
    irr = None
    # START CODE HERE

    # END CODE HERE
    return irr


def optimal_investment(cashflows: dict, **kwargs) -> str:
    """
    Finds the optimal across multiple cashflow schedules of investments.
    The optimality is determined based on the IRR found using the above functions.

    Args:
    ------
        cashflows (dict): A dict of dicts containing cashflow schedules for a set of investments.
        
    Returns:
    --------
        str: The dictionary key of the optimal investment in the input dict
    """
    key = None
    # START CODE HERE

    # END CODE HERE
    return key
