# Technical Assessment

This repository contains the instructions, code and submission files for Qtx/Chisl technical assesment.

It consists of three distinct tasks, these being:

- [ ]	Descriptive analytics (Task A)
- [ ]	Client advisement task (Task B)
- [ ]	Computational challenges (Task C)
- [ ]   Web development task (Task D)

## Submission Guidelines

Please note the following notes on submission format and style:
-	Please complete all tasks in `Python`
-	Please provide a separate Jupyter notebook for each of the tasks that require a notebook submission
-	Please ensure that your submission is self-contained – i.e. if we run your notebook, python scripts, we should be able to replicate your results without having to set data paths or install packages
-	Please comment your code to clearly demonstrate your approach and logic. Clear communication and legibility are just as, if not more, important than efficient coding and correct results
-	Please present your code professionally as if we are a client requesting the above tasks 
-   Assume that the client will have access to your code
-   Written submissions should be made in the `.md` markdown files provided
-	Feel free to reach out for clarification if required

### Once the submission is completed

* Either raise a merge request with the `main` branch and notify one of the `repo` owners.
* Or send an email to the contact person with a zip file of your repo branch (with your submission).


## Getting Started

Each folder in the repository contains the Tasks description (in a README) and the relevant files and/or data.

1. Clone the git repository
2. Create your own **new** branch with the name convention: `name_surname`


