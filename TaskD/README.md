# Task D: Web development 

An Insurer has requested a simple feedback form to assist them in their CRM process. After meeting with the client we have dervied the following user stories from their specifications.

**User stories:**

1. They would like their policy holders (customers) to be able to rate the quality of their service in order to guage the overall quality of their service across all clients.
2. They would like general feedback from their customers so that they can investigate poor ratings.
3. They would like to see a historical log of all these ratings and the relevant feedback.
4. The ratings and feedback can be anonymous.

**Note:** The historical feedback should only be visible to the Insurer (and not all other customers). This can just be a generic password protected view.

**Example of UI:**

See the image in the repo.

**Technical specifications:**

- [ ] Use React >= 18
- [ ] Use tailwindcss for the styling
- [ ] Use a .json for the feedback database history


