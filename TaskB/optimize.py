
import numpy as np
import scipy 


def optimal_portfolio(return_stats: np.array, cov_mat: np.array) -> np.array:
    """
    This function takes in the return profile statistics of a set of investments and returns a dictionary.
    The dictionary contains the portfolio weights, returns and variance.

    Args:
    ------
        mean_returns(np.array): A 2 x N matrix, where the first row is the mean (expected return) and 
        the second row is the variance, for N investments.
        cov_mat (np.array): c x c covariance matrix of the investment denominated currencies.
    
    Returns:
        dict: portfolio statistics with keys `weights`, `expected_returns` and `variance_returns` 

    """
    portfolio_stats = None
    # START CODE HERE

    # END CODE HERE
    return portfolio_stats

